# easy-chroot

A wrapper for chroot - adapted from arch-chroot

The script is a slightly simplified version of of the Arch Linux `arch-chroot`
script. It adds the option to specify the shell to use in chroot'ing via the
`-s` option.

Versioning follows that of arch-chroot, i.e. versions `X.Y` are derived from
arch-chroot version `X`.
